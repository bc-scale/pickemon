"""
This module provides the standard model for Pokemon.
"""


class PokemonNotFound(RuntimeError):
    """
    Pokemon not found exception.
    """
    pass


class Pokemon(object):
    """
    Models Pokemon.
    """

    def __init__(self, name: str, description: str, habitat: str, is_legendary: bool) -> None:
        """
        Initializes the Pokemon.

        :param name: the name
        :param description: the description
        :param habitat: the habitat
        :param is_legendary: the legendary status
        """
        self.name = name
        self.description = description
        self.habitat = habitat
        self.is_legendary = is_legendary
